#!/usr/bin/python3

import ipaddress
import os
import re
import sys

import MySQLdb

BLACKLIST = [
    "bitchute",
    "bitcoin",
    "drugs",
    "hacking",
    "loan",
    "movie",
    "newsbreak",
    "paypal",
    "porn",
    "support",
    "western union",
    "westernunion",
    "wplms",
]


def check_if_spam(data):
    lines = data.splitlines()

    for word in BLACKLIST:
        if re.search(word, data, re.IGNORECASE):
            return True

    # Delete paste if contains more than 2 lines containing an URL
    link_count = 0
    for line in lines:
        if re.search("http[s]?://", line, re.IGNORECASE):
            link_count += 1
        elif re.search("html$", line, re.IGNORECASE):
            link_count += 1

    if link_count >= 2:
        return True

    # Delete paste if contains more than 2 IP addresses
    ip_count = 0
    for line in lines:
        try:
            if ipaddress.ip_address(line.split(":")[0]):
                ip_count += 1
        except ValueError:
            pass

    if ip_count >= 2:
        return True

    # Spammers tend to paste the same URL n times
    firstline = lines[0]
    repeat_counter = lines.count(firstline)
    if repeat_counter > 1 and len(lines) == repeat_counter:
        return True

    return False


def main():
    try:
        hostname = os.environ["STICKYNOTES_DB_HOSTNAME"]
        username = os.environ["STICKYNOTES_DB_USERNAME"]
        password = os.environ["STICKYNOTES_DB_PASSWORD"]
        dbname = os.environ["STICKYNOTES_DB_NAME"]
    except KeyError:
        sys.exit(1)

    db = MySQLdb.connect(host=hostname, user=username, password=password, db=dbname)
    with db.cursor() as c:
        c.execute("""SELECT urlkey, data FROM main""")
        pastes = c.fetchall()

    for paste in pastes:
        urlkey, data = paste
        if check_if_spam(data):
            with db.cursor() as c:
                print("deleting {}".format(urlkey))
                c.execute("""DELETE from main WHERE urlkey = %s""", (urlkey,))

    db.commit()
    db.close()


if __name__ == "__main__":
    main()
