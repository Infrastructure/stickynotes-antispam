FROM fedora:30

RUN dnf install -y python3-mysql
ADD stickynotes-antispam.py /usr/local/bin/pastespam
ENTRYPOINT ["/usr/local/bin/pastespam"]
